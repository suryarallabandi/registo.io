<?php
 
if($_POST) {
    $voucher = "";
    $date = "";
    $name = "";
    $sonof = "";
    $mode = "";
    $amount = "";
    $txid = "";
    $course = "";
    $institute = "";
    $email = "";


    if(isset($_POST['voucher'])) {
        $voucher = filter_var($_POST['voucher'], FILTER_SANITIZE_STRING);
    }
    if(isset($_POST['date'])) {
        $date = filter_var($_POST['date'], FILTER_SANITIZE_NUMBER_INT);
    }
     
    if(isset($_POST['name'])) {
        $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
    } 
    if(isset($_POST['sonof'])) {
        $sonof = filter_var($_POST['sonof'], FILTER_SANITIZE_STRING);
    }
     
    if(isset($_POST['mode'])) {
        $mode = filter_var($_POST['mode'], FILTER_SANITIZE_STRING);
    }
    if(isset($_POST['amount'])) {
        $amount = filter_var($_POST['amount'], FILTER_SANITIZE_NUMBER_INT);
    }
    if(isset($_POST['course'])) {
        $course = filter_var($_POST['course'], FILTER_SANITIZE_STRING);
    }
    if(isset($_POST['txid'])) {
        $txid = filter_var($_POST['txid'], FILTER_SANITIZE_STRING);
    }
    if(isset($_POST['institute'])) {
        $institute = filter_var($_POST['institute'], FILTER_SANITIZE_STRING);
    }
    if(isset($_POST['email'])) {
        $email = str_replace(array("\r", "\n", "%0a", "%0d"), '', $_POST['email']);
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
         
    }

    $from = $email;
    $to = "vidyabharati.desk@gmail.com";
     
    $headers  = 'MIME-Version: 1.0' . "\r\n"
    .'Content-type: text/html; charset=utf-8' . "\r\n"
    .'From: ' . $email . "\r\n";

    $headers1  = 'MIME-Version: 1.0' . "\r\n"
    .'Content-type: text/html; charset=utf-8' . "\r\n"
    .'From: ' . $to . "\r\n";
 
    $email_content = "<html><body>";
    $email_content .= "<table style='font-family: Arial;'><tbody><tr><td style='background: #eee; padding: 10px;'>Voucher No</td><td style='background: #fda; padding: 10px;'>$voucher</td></tr>";
    $email_content .= "<tr><td style='background: #eee; padding: 10px;'>Date Of Payemnt</td><td style='background: #fda; padding: 10px;'>$date</td></tr>";
    $email_content .= "<tr><td style='background: #eee; padding: 10px;'>Candidate Name</td><td style='background: #fda; padding: 10px;'>$name</td></tr>";
    $email_content .= "<tr><td style='background: #eee; padding: 10px;'>Father Name</td><td style='background: #fda; padding: 10px;'>$sonof</td></tr>";
    $email_content .= "<tr><td style='background: #eee; padding: 10px;'>Mode Of Payment</td><td style='background: #fda; padding: 10px;'>$mode</td></tr>";
    $email_content .= "<tr><td style='background: #eee; padding: 10px;'>Amount</td><td style='background: #fda; padding: 10px;'>$amount</td></tr>";
    $email_content .= "<tr><td style='background: #eee; padding: 10px;'>Course</td><td style='background: #fda; padding: 10px;'>$course</td></tr>";
    $email_content .= "<tr><td style='background: #eee; padding: 10px;'>Institute</td><td style='background: #fda; padding: 10px;'>$institute</td></tr></tbody></table>";
 
    $email_content .= "<p style='font-family: Arial; font-size: 1.25rem;'><strong>Email Address</strong><i> $email</i>.</p>";
    $email_content .= '</body></html>';

    $email_content1 = "<html style='background: #F8F9F9;font-family: verdana;'><body>";
    $email_content1 .= "<center><img src='http://registoservice.rf.gd/logo.png' style='width: 100px;height: 100px;'/></center>";
    $email_content1 .= "<h1 style='text-align:center;'>Vidya Bharati Institute Of Management & Technology</h1>"; 
    $email_content1 .= "<p style='text-align:center;'>Address: B-88, Ground Floor, Sector-64, Noida-201301 (U.P.)</p>"; 
    $email_content1 .= "<div style='clear:both'>";
    $email_content1 .= "<div style='padding: 10px;width: 80%;margin: 0 auto;'>";
    $email_content1 .= "<div style='float:left;'>";
    $email_content1 .= "<p><b>Voucher No.</b> : $voucher</p>";
    $email_content1 .= "</div>";
    $email_content1 .= "<div style='float:right;'>";
    $email_content1 .= "<p><b>Date</b> : $date</p>";
    $email_content1 .= "</div>";
    $email_content1 .= "<div style='clear:both'>";
    $email_content1 .= "<p><b>Name</b> : $name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>S/o</b> : $sonof &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Has Paid Through Cheque/DD No.</b> : $mode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Cash Rs.</b> : &#x20B9; $amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Transaction ID</b> : $txid &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Course</b> : $course &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Institute</b> : $institute &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Date</b> : $date</p>";
    $email_content1 .= "</div>";
    $email_content1 .= "<div style='clear:both'>";
    $email_content1 .= "<center><span>*This is an online generated receipt, does not require signature.</span></center>";
    $email_content1 .= "<div style='clear:both'>";
    $email_content1 .= "<center><span style='text-align: center;color: red;'>*18% GST will be applicable on all fees paid</span></center>";
    $email_content1 .= "</body></html>";
 
    echo $email_content;
    echo $email_content1;
     
    if(mail($to, "Payment Slip.", $email_content, $headers)) {
        mail($from, "Payment Slip.", $email_content1, $headers1);
        echo '<p>Receipt sent successfully.</p>';
    } else {
        echo '<p>Error: Not sent, Try again.</p>';
    }
     
}
?>
