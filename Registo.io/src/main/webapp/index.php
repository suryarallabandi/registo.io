<!DOCTYPE html>
<html>
	<head>
		<title>Sample user registration for devops project</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
	</head>
	<body>
		<div class="header">
			<a href="index.php"><h2>Registo.io</h2></a>
		</div>
		<div style="clear:both"></div>
		<div class="container">
			<div class="receipt" style="margin-top: 10%;">
				<form action="mail.php" method="post">
					<input type="text" name="voucher" id="voucher" placeholder="Voucher No">
					<input type="date" name="date" id="date" placeholder="Date Of Payment">
					<input type="text" name="name" id="name" placeholder="Candidate Name">
					<input type="text" name="sonof" id="sonof" placeholder="Father Name">
					<input type="text" name="mode" id="mode" placeholder="Mode Of Payment">
					<input type="text" name="amount" id="amount" placeholder="Amount">
					<input type="text" name="course" id="course" placeholder="Course">
					<input type="text" name="institute" id="institute" placeholder="Institute">
					<input type="email" name="email" id="email" placeholder="Email Address">
					<input type="text" name="txid" id="txid" placeholder="Transaction ID">
					<input type="submit" name="submit" id="submit" value="Send Receipt">
				</form>
			</div>
		</div>
		<div style="clear:both"></div>
	</body>
</html>
